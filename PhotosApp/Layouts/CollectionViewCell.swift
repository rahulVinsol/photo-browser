//
//  CollectionViewCell.swift
//  PhotosApp
//
//  Created by Rahul Rawat on 25/06/21.
//

import UIKit
import Photos

class CollectionViewCell: UICollectionViewCell {
    static let IDENTIFIER = "cell"
    
    var isEditing = false {
        didSet {
            editCheckImageView?.isHidden = !isEditing
        }
    }
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var editCheckImageView: UIImageView!
    
    func setupView(isEditing: Bool, isSelected: Bool, with asset: PHAsset, size: CGSize) {
        self.isEditing = isEditing
        self.isSelected = isSelected
        
        PHImageManager.default().requestImage(for: asset, targetSize: size, contentMode: .aspectFill, options: nil) { [weak self] (image: UIImage?, info: [AnyHashable: Any]?) -> Void in
            guard let self = self, let image = image else { return }
            
            self.imageView.image = image
        }
    }
    
    override var isSelected: Bool {
        didSet {
            editCheckImageView?.image = UIImage(systemName: isSelected ? "checkmark.circle" : "circle")
        }
    }
}
