//
//  ViewController.swift
//  PhotosApp
//
//  Created by Rahul Rawat on 25/06/21.
//

import UIKit
import Photos

class ViewController: UIViewController {
    private let itemsPerRow: CGFloat = 3
    private let sectionInsets = UIEdgeInsets(top: 50.0, left: 20.0, bottom: 50.0, right: 20.0)
    private lazy var widthPerItem: CGFloat = {
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        return availableWidth / itemsPerRow
    }()
    private weak var customGalleryLayout: CustomGalleryLayout?
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    private var galleryImages: PHFetchResult<PHAsset>?
    private var selectedIndex: Int?
    
    private var slideShowButton: UIBarButtonItem!
    private var cancelButton: UIBarButtonItem!
    
    private var selectedIndexes: Set<Int> = []
    private var assets = [PHAsset]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        if #available(iOS 14, *) {
            PHPhotoLibrary.requestAuthorization(for: .readWrite) { [weak self] (status) in
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else { return }
                    self.showUI(for: status)
                }
            }
        } else {
            PHPhotoLibrary.requestAuthorization { [weak self] status in
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else { return }
                    self.showUI(for: status)
                }
            }
        }
        
        PHPhotoLibrary.shared().register(self)
        
        if let layout = collectionView?.collectionViewLayout as? CustomGalleryLayout {
            customGalleryLayout = layout
            layout.delegate = self
        }
        
        slideShowButton = UIBarButtonItem(image: UIImage(systemName: "play.rectangle"), style: .plain, target: self, action: #selector(chooseSlideshowImages))
        cancelButton = UIBarButtonItem(image: UIImage(systemName: "xmark"), style: .plain, target: self, action: #selector(cancelPressed))
        
        navigationItem.rightBarButtonItem = slideShowButton
    }
    
    @objc func cancelPressed() {
        setEditing(false, animated: true)
    }
    
    @objc func chooseSlideshowImages() {
        if isEditing {
            assets.removeAll()
            
            if let galleryImages = galleryImages {
                for index in selectedIndexes {
                    assets.append(galleryImages[index])
                }
            }
            
            if !assets.isEmpty {
                performSegue(withIdentifier: "toSlideShow", sender: self)
            }
                
            setEditing(false, animated: true)
        } else {
            setEditing(true, animated: true)
        }
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        
        selectedIndexes.removeAll()
        
        slideShowButton.image = editing ? UIImage(systemName: "play") : UIImage(systemName: "play.rectangle")
        collectionView.allowsMultipleSelection = editing
        collectionView.reloadData()
        
        navigationItem.leftBarButtonItem = editing ? cancelButton : nil
    }
    
    private func showUI(for status: PHAuthorizationStatus) {
        switch status {
        case .authorized, .limited:
            showPhotos()
            
        case .restricted:
            
            break
            
        case .denied:
            showAccessDeniedUI()
            
        case .notDetermined:
            break
            
        default:
            break
        }
    }
    
    private func showRestrictedUI() {
        let alert = UIAlertController(title: "Restricted", message: "Phone in Restricted mode can't do anything.", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Okay", style: .default, handler: nil)
        alert.addAction(okAction)
        
        present(alert, animated: true, completion: nil)
    }
    
    private func showAccessDeniedUI() {
        let alert = UIAlertController(title: "Allow access to your photos", message: "Go to your settings and tap \"Photos\".", preferredStyle: .alert)
        
        let notNowAction = UIAlertAction(title: "Not Now", style: .cancel, handler: nil)
        alert.addAction(notNowAction)
        
        let openSettingsAction = UIAlertAction(title: "Open Settings", style: .default) { [weak self] (_) in
            self?.gotoAppPrivacySettings()
        }
        alert.addAction(openSettingsAction)
        
        present(alert, animated: true, completion: nil)
    }
    
    private func gotoAppPrivacySettings() {
        guard let url = URL(string: UIApplication.openSettingsURLString),
              UIApplication.shared.canOpenURL(url) else {
            return
        }
        
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    private func showPhotos() {
        galleryImages = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: nil)
        collectionView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "showPhotos":
            let vc = segue.destination as! PageViewController
            vc.currentIndex = selectedIndex!
            vc.galleryImages = galleryImages
        case "toSlideShow":
            let vc = segue.destination as! SlideShowPageViewController
            vc.assets = assets
        default:
            break
        }
    }
    
    deinit {
        PHPhotoLibrary.shared().unregisterChangeObserver(self)
    }
}

extension ViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return galleryImages?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCell.IDENTIFIER, for: indexPath) as! CollectionViewCell
        
        let index = indexPath.row
        if let image = galleryImages?[index] {
            cell.setupView(isEditing: isEditing, isSelected: selectedIndexes.contains(indexPath.row), with: image, size: getAvailableSize(for: indexPath))
        }
        
        return cell
    }
    
    private func getAvailableSize(for indexPath: IndexPath) -> CGSize {
        guard let size = customGalleryLayout?.layoutAttributesForItem(at: indexPath)?.size  else { return CGSize.zero }
        
        return size
    }
}

extension ViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if isEditing {
            selectedIndexes.insert(indexPath.row)
        } else {
            selectedIndex = indexPath.row
            performSegue(withIdentifier: "showPhotos", sender: self)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        selectedIndexes.remove(indexPath.row)
    }
}

extension ViewController: PHPhotoLibraryChangeObserver {
    func photoLibraryDidChange(_ changeInstance: PHChange) {
        DispatchQueue.main.async { [weak self] in
            let status: PHAuthorizationStatus
            if #available(iOS 14, *) {
                status = PHPhotoLibrary.authorizationStatus(for: .readWrite)
            } else {
                status = PHPhotoLibrary.authorizationStatus()
            }
            self?.showUI(for: status)
        }
    }
}

extension ViewController: CustomGalleryLayoutDelegate {
    func collectionView(_ collectionView: UICollectionView, widthAtIndexPath width: CGFloat, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
        
        let index = indexPath.row
        guard let asset = galleryImages?[index] else { return CGFloat.zero }
        
        let height = width * CGFloat(asset.pixelHeight) / CGFloat(asset.pixelWidth)
        
        return height
    }
}
