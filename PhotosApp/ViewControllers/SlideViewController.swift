//
//  SlideViewController.swift
//  PhotosApp
//
//  Created by Rahul Rawat on 28/06/21.
//

import UIKit
import Photos

class SlideViewController: UIViewController {

    @IBOutlet weak var vStack: UIStackView!
    
    var index: Int!
    var assets: [PHAsset]! {
        didSet {
            loadAssets()
        }
    }
    
    private var images: [UIImage] = []
    private var assetProcessedCount = 0 {
        didSet {
            if assetProcessedCount == assets.count {
                setSpaces()
            }
        }
    }
    
    private func loadAssets() {
        let screenWidth = UIScreen.main.bounds.width
        
        for asset in assets {
            let ar = CGFloat(asset.pixelWidth) / CGFloat(asset.pixelHeight)
            
            let height = screenWidth / ar
            
            let requestOptions = PHImageRequestOptions()
            requestOptions.resizeMode = PHImageRequestOptionsResizeMode.exact
            requestOptions.deliveryMode = PHImageRequestOptionsDeliveryMode.highQualityFormat
            requestOptions.isSynchronous = false
            
            if (asset.mediaType == PHAssetMediaType.image) {
                let cgSize = CGSize(width: screenWidth, height: height)
                PHImageManager.default().requestImage(for: asset, targetSize: cgSize, contentMode: PHImageContentMode.default, options: requestOptions, resultHandler: { [weak self] (pickedImage, info) in
                    guard let self = self else {
                        return
                    }
                    
                    guard let pickedImage = pickedImage else {
                        self.assetProcessedCount += 1
                        return
                    }
                    
                    if self.isViewLoaded {
                        self.addImageView(image: pickedImage)
                    } else {
                        self.images.append(pickedImage)
                    }
                })
            } else {
                assetProcessedCount += 1
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .clear
    
        for image in images {
            addImageView(image: image)
        }
        
        images.removeAll()
    }
    
    private func addImageView(image: UIImage) {
        let imageView = UIImageView()
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        
        vStack.addArrangedSubview(imageView)
        
        assetProcessedCount += 1
    }
    
    private func setSpaces() {
        var totalHeight = CGFloat.zero
        for subview in vStack.subviews {
            if let size = (subview as? UIImageView)?.image?.size {
                totalHeight += size.height
            }
        }
        
        let screenWidth = UIScreen.main.bounds.width
        let remainingHeight = (UIScreen.main.bounds.height - totalHeight) / 2
        
        var spacer = UIView()
        spacer.backgroundColor = .clear
        spacer.heightAnchor.constraint(equalToConstant: remainingHeight).isActive = true
        spacer.widthAnchor.constraint(equalToConstant: screenWidth).isActive = true
        
        vStack.insertArrangedSubview(spacer, at: 0)
        
        spacer = UIView()
        spacer.backgroundColor = .clear
        spacer.heightAnchor.constraint(equalToConstant: remainingHeight).isActive = true
        spacer.widthAnchor.constraint(equalToConstant: screenWidth).isActive = true
        
        vStack.addArrangedSubview(spacer)
    }
}
