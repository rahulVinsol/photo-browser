//
//  SlideShowPageViewController.swift
//  PhotosApp
//
//  Created by Rahul Rawat on 28/06/21.
//

import UIKit
import Photos

class SlideShowPageViewController: BasePageViewController {
    
    var assets: [PHAsset]!
    
    private var currentIndex: Int = 0
    private var nextIndex: Int?
    
    override var defaultMode: BasePageViewController.ScreenMode { get { .full } }
    
    private var vcs = [SlideViewController]()
    
    private var timer: Timer!
    private var counter = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let screenBounds = UIScreen.main.bounds
        let screenWidth = screenBounds.width
        let screenHeight = screenBounds.height
        
        var assetsForVC = [PHAsset]()
        var availableHeight = screenHeight
        var initialVC: SlideViewController?
        
        for image in assets {
            let ar = CGFloat(image.pixelWidth) / CGFloat(image.pixelHeight)
            
            let height = screenWidth / ar
            
            if availableHeight - height <= 0 {
                appendVcs(assets: assetsForVC)
                
                availableHeight = screenHeight
                
                assetsForVC.removeAll()
            }
            assetsForVC.append(image)
            availableHeight -= height
        }
        
        if !assetsForVC.isEmpty {
            appendVcs(assets: assetsForVC)
        }
        
        initialVC = vcs.first
        
        if let initialVC = initialVC {
            setViewControllers([initialVC], direction: .forward, animated: true, completion: nil)
        }
        
        if vcs.count > 1 {
            timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
        }
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donePressed))
    }
    
    @objc func timerAction() {
        counter += 1
        if counter < vcs.count {
            currentIndex = counter
            setViewControllers([vcs[counter]], direction: Bool.random() ? .forward : .reverse, animated: true, completion: nil)
        } else {
            timer.invalidate()
            changeScreenMode(to: .normal)
        }
    }
    
    @objc func donePressed() {
        navigationController?.popViewController(animated: true)
    }
    
    private func appendVcs(assets: [PHAsset]) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "\(SlideViewController.self)") as! SlideViewController
        vc.assets = assets
        vc.index = vcs.count
        vcs.append(vc)
    }
    
    deinit {
        if timer.isValid {
            timer.invalidate()
        }
    }
}
